apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "calibre-web.fullname" . }}
  labels:
    {{- include "calibre-web.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "calibre-web.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "calibre-web.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "calibre-web.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            {{- if .Values.env.DOCKER_MODS }}      
            - name: DOCKER_MODS
              value: linuxserver/mods:universal-calibre    
            {{- end }}  
            - name: PUID
              value: "{{ .Values.env.PUID }}"
            - name: PGID
              value: "{{ .Values.env.PGID }}"
            - name: TZ
              value: {{ .Values.env.TZ }}
          ports:
            - name: http
              containerPort: 8083
              protocol: TCP
          livenessProbe:
            initialDelaySeconds: 200      
            httpGet:
              path: /
              port: http
          readinessProbe:
            initialDelaySeconds: 5
            httpGet:
              path: /
              port: http
          volumeMounts:
            - name: calibre-books
              mountPath: /books
            - name: calibre-config
              mountPath: /config
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: calibre-books
          {{- if .Values.persistence.data.enabled }}
          persistentVolumeClaim:
            {{- if .Values.persistence.data.claimName }}
            claimName: "{{ .Values.persistence.data.claimName }}"
            {{- else }}
            claimName: "{{ template "calibre-web.fullname" . }}-data"
            {{- end }}
          {{- else }}  
          configMap:
            name: metadata.db-books
          {{- end }}            
        - name: calibre-config
          persistentVolumeClaim:
            {{- if .Values.persistence.config.claimName }}
            claimName: "{{ .Values.persistence.config.claimName }}"
            {{- else }}
            claimName: "{{ template "calibre-web.fullname" . }}-config"
            {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
